
public class Main {

	public static void main(String[] args) {
		Library lib = new Library();
		Student stu = new Student("Napatpong","5610450951","D14");
		//getName don't change new data
		Book book1 = new Book("Big Java");
		Book book2 = new Book("PPL");
		ReferenceBook refBook = new ReferenceBook("Java Reference");
		//Reference book can't be borrowed
		lib.addBook(book1);
		lib.addBook(book2);
		lib.addRefBook(refBook);
		System.out.println(lib.getBookCount()); //3
		lib.borrowBook(stu,"PPL");  // `borrw 1 book >> 3-1
		System.out.println(lib.getBookCount()); //2
		lib.returnBook(stu,"PPL"); //return book 2+1
		System.out.println(lib.getBookCount()); //3
		lib.borrowBook(stu,"Java reference");
		System.out.println(lib.getBookCount()); //3
		System.out.println(stu.getName()); //Da Ruttanagorn

	}

}
